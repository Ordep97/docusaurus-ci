FROM node:17-alpine3.14

EXPOSE 3000

WORKDIR /my-website

RUN yes;npx create-docusaurus@latest my-website classic
RUN apk add bash
CMD cd my-website;npm run start